#!/bin/bash
set -euo pipefail

function usage {
    echo "usage: download.sh <target_dir>"
    exit 1
}

if [ $# -ne 1 ]; then
    usage
fi
TARGET_DIR=$1

if [ ! -d $TARGET_DIR ]; then
    echo "Target dir not found: $TARGET_DIR"
    usage
fi


echo "download data to target dir: $TARGET_DIR"

# Your code here
wget "$ZIP_URL" -O $TARGET_DIR/$ZIP_FILE_NAME

echo "done."