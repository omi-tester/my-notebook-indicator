# Jupyter notebook indicator template

An indicator project template based on the use of Jupyter notebook ready to be used by the pipeline

Fork it and adapt to your needs!

## Local install

Preferably use a virtual env:

```bash
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
pip install -r requirements-dev.txt
```

Run notebook:

```bash
jupyter-lab notebook.ipynb
```

## Using Docker

Common env variables

```bash
DOCKER_IMAGE_NAME=jupyter-notebook:latest
DOWNLOAD_DIR=/path/to/download
COMPUTE_DIR=/path/to/compute
PROFILE_ENV_FILE=parameter_profiles/indre.env
```

Build container image

```bash
docker build -t $DOCKER_IMAGE_NAME .
```

Run download:

```bash
docker run -v $DOWNLOAD_DIR:/data/download \
       $DOCKER_IMAGE_NAME --env-file $PROFILE_ENV_FILE /app/download.sh /data/download
```

Run compute:

```bash
docker run -v $DOWNLOAD_DIR:/data/download -v $COMPUTE_DIR:/data/compute \
       $DOCKER_IMAGE_NAME --env-file $PROFILE_ENV_FILE /app/compute.sh /data/download /data/compute
```
